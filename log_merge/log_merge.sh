#!/bin/bash

# 引数個数チェック START
echo "start argument check........."
if [ $# -ne 1 ]; then
    echo "abend argument check........."
    echo "実行するには1個の引数が必要です。" 1>&2
    echo "指定可能な引数は [ tatenashi|sbisec|ssnb|api|k2 ] です" 1>&2
    exit 1
fi

# ログの種類が増えたら配列に許容するprefix増やせばおｋ
args=(tatenashi sbisec ssnb api k2)
exists=0
for prefix in ${args[@]}*; do
    if [ $prefix = $1 ]; then
        exists=1
        break
    fi
done
if [ ${exists} -eq 0 ]; then
    echo "abend argument check........."
    echo "指定可能な引数は [ tatenashi|sbisec|ssnb|api|k2 ] です" 1>&2
    exit 1
fi
echo "end   argument check........."
# 引数個数チェック END

WORK_DIR=`dirname $0`
DATESTR=`date '+%Y%m%d_%T' | tr -d :`
cd $WORK_DIR

# 前回マージログの削除
rm -Rf Access*.log
rm -Rf tatenashi*.log
rm -Rf iis*.log

# 前回処理の残骸削除
if [ -e unzip ]; then
    rm -Rf unzip
fi

# 展開用ディレクトリ作成
mkdir unzip

# サーバごとのディレクトリで展開
echo "start unzip........."
for FILE in ${DIRPATH}*; do
    EXTNAME=${FILE##*.}
    if [ ${EXTNAME} = 'zip' ]; then
        # 先ほど取得した FILENAME から、拡張子を除いたファイル名を取得します。
        FILENAME_WITHOUT_EXT=${FILE%.*}
        DIR_NAME=${FILENAME_WITHOUT_EXT##*_}
        unzip -q ${FILE} -d unzip/${DIR_NAME}
    fi
done
echo "end   unzip........."

# ファイル一覧取得
files=unzip/*
dirary=()
for filepath in $files; do
  if [ -d $filepath ] ; then
    dirary+=("$filepath")
  fi
done

# 見ないログ削除
rm -Rf unzip/*/tomcat8*
rm -Rf unzip/*/gc.log*
rm -Rf unzip/*/isapi_redirect.log*

# Access、Tatenashi、iisを結合

echo "start log merge........."
for dir in ${dirary[@]}; do
    cat $dir/Access* | grep -v HealthChecker >> Access.merge.${DATESTR}.log
    sort -k1 $dir/$1* >> tatenashi.merge.tmp.log
    cat $dir/iis* | grep -v ELB-HealthChecker | grep -v "/js/" | grep -v "/css/" | grep -v "/image" >> iis.merge.${DATESTR}.log
done

# AccessLogを並べ替え
sort -k1 tatenashi.merge.tmp.log >> tatenashi.merge.${DATESTR}.log
echo "end   log merge........."

# tmpログ削除
rm -Rf tatenashi.merge.tmp.log

# 展開したフォルダもいらない
rm -Rf unzip

########## TODO ##########
